<?php /* Smarty version Smarty-3.0.8, created on 2014-03-30 15:12:48
         compiled from "E:\projects\PHP\PFinal\PFinal\src\webroot\template\dashboard.html" */ ?>
<?php /*%%SmartyHeaderCode:2513053383470d15b77-28352977%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15d85863c8dd0143355e2e70e9f2091d3cdd4f25' => 
    array (
      0 => 'E:\\projects\\PHP\\PFinal\\PFinal\\src\\webroot\\template\\dashboard.html',
      1 => 1396192365,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2513053383470d15b77-28352977',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("header.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<link rel="stylesheet" type="text/css" href="/css/dashboard.css">
<script type="text/javascript" src="/js/dashboard.js"></script>
	<div class="container new">
		<form class="form-inline" role="form" action="/index.php/account/addwx">
			<div class="form-group">
				<label class="sr-only" for="wxName">微信账号></label>
				<input class="form-control" id="wxName" name="wxName" placeholder="微信账号">
			</div>
			<div class="form-group">
				<label class="sr-only" for="wxPasswd">微信密码></label>
				<input class="form-control" id="wxPasswd" name="wxPasswd" placeholder="微信密码">
			</div>
			<button type="submit" id="addWxAccout" class="btn btn-primary">新增</button>
		</form>
	</div>
	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>微信账号</th>
					<th>微信密码</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<?php  $_smarty_tpl->tpl_vars['account'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('wxAccounts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['account']->key => $_smarty_tpl->tpl_vars['account']->value){
?>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['account']->value['id'];?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['account']->value['name'];?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['account']->value['password'];?>
</td>
						<td><a href="/index.php/account/setpos?id=<?php echo $_smarty_tpl->tpl_vars['account']->value['id'];?>
" target="_blank">操作</a></td>
					</tr>
				<?php }} ?>
			</tbody>
		</table>
	</div>
<?php $_template = new Smarty_Internal_Template("footer.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>