<?php
define('PRODUCT', 'PFinal');
define('PROJECT', dirname(dirname(dirname(__FILE__))));
define('ROOT', dirname(dirname(__FILE__)));
define('WEB_ROOT', dirname(__FILE__).DIRECTORY_SEPARATOR);
define('APPLICATION', ROOT.DIRECTORY_SEPARATOR.'application');
define('LIB', ROOT.DIRECTORY_SEPARATOR.'lib');
define('CONTROLLER', APPLICATION.DIRECTORY_SEPARATOR.'controller');

define('LOG_PATH', PROJECT.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR);


set_include_path(get_include_path().PATH_SEPARATOR.implode(PATH_SEPARATOR,array(
APPLICATION,
LIB,
)));

error_reporting(E_ERROR);

require_once 'PFinal/PFinal.class.php';

PFinal::run('application.JFinalConfig');