<?php
class JFinalConfig  extends Pfinal_Config_Base {
	/*
	 * (non-PHPdoc) @see Pfinal_Config_Base::configConstant()
	 */
	public function configConstant(Pfinal_Config_Constant $me) {
		// TODO Auto-generated method stub
		$me->setDevMode ( Pfinal_Config_Constant::MODE_DEV );
		$me->setUrlSeparator('-');
		$me->setInterceptorMode(Pfinal_Config_Constant::MODE_INNOTATION);
		$me->setViewType(Pfinal_Config_Constant::VIEW_SMARTY);
		$me->setOrmEnable(true);
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Config_Base::configRoute()
	 */
	public function configRoute(Pfinal_Config_Route $me) {
		// TODO Auto-generated method stub
		$me->addRoute (new Pfinal_Route_Default());
	//	$me->addRoute(new MyRegex_Router());
	//	$me->addRoute(new MyPrefix_Test_Router());
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Config_Base::configPlugin()
	 */
	public function configPlugin(Pfinal_Config_Plugin $me) {
		// TODO Auto-generated method stub
		$me->add(new Log_Util_Plugin());
		$me->add(new Smarty_View_Plugin());
		//错误处理
	//	$me->add(new Pfinal_Plugin_ExceptionHandler());
	//	$me->add(new Pfinal_Plugin_ErrorHandler());

		$wx = new Pfinal_Plugin_DbConnection(array('host'=>'127.0.0.1','user'=>'root','password'=>'demo','database'=>'wx'));
		$me->add($wx);
		$activeRecord = new Pfinal_Plugin_ActiveRecord();
		$activeRecord->addOrmMapping('wx_user', 'User_Entity');
		$activeRecord->addOrmMapping('wx_account', 'WxAccount_Entity');
		$activeRecord->add('wx', $wx);
	
		$me->add($activeRecord);
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Config_Base::configInterceptor()
	 */
	public function configInterceptor() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Config_Base::configHandler()
	 */
	public function configHandler() {
		// TODO Auto-generated method stub
	}
}

?>