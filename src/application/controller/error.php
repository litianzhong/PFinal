<?php
class ErrorController extends Pfinal_Controller_Abstract{
	
	public function index(){
		$this->httpResponse->set('message',$this->httpRequest('err'));
		$this->renderHtml('500.html');
	}
}