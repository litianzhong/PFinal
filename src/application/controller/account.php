<?php
/**
 * 
 */
class AccountController extends Pfinal_Controller_Abstract{

	protected $userService;

	public function __construct($httpRequest){
		parent::__construct($httpRequest);
		$this->userService = new User_Service();
	}

	public function addwx(){

		$accout = $this->httpRequest->request('wxName');
		$password = $this->httpRequest->request('wxPasswd');
		$userId = 1;
		$this->userService->addWxAccount($userId,$accout,$password);
		$this->redirect("/welcome/dashboard");
	}

	public function setpos(){


		$this->renderHtml('operator.html');
	}

}