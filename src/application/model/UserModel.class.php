<?php

class User_Model extends PfinalModelAbstract{

	public function __construct(){
		$this->tableName = 'wx_user';
		$this->databaseName = 'wx';
		parent::__construct();
	}

	public function getByEmail($email){
		$users = $this->fetchAll(array('email=?'=>$email));
		if (!empty($users)) {
			return $users[0];
		}else{
			return null;
		}
	}

	/**
	 * [addUser description]
	 * @param [type] $email    [description]
	 * @param [type] $password [description]
	 */
	public function addUser($email,$password){
		$userId = $this
			->set('email',trim($email))
			->set('passwd',md5(trim($password)))
			->set('status',User_Entity::STATUS_OK)
			->set('createTime',date('Y-m-d H:i:s'))
			->save();
		return $userId;
	}
}