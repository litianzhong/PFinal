<?php
class My_Interceptor implements Pfinal_Interceptor_Interface {
	/*
	 * (non-PHPdoc) @see Pfinal_Interceptor_Interface::interceptor()
	 */
	public function interceptor(Pfinal_Invocation_Handler $invocation) {
		// TODO Auto-generated method stub
		$invocation->invoke();
	
		$controller = $invocation->getInstance();
		$httpResponse = $controller->getHttpResponse();
		$httpResponse->set('key',__CLASS__);
		
		$render = $controller->getRender();
		
		$render->setHttpResponse($httpResponse);
		$render->render();
		//print_r($render);
	}
}

?>