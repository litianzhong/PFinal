<?php
/**
 * @author huangyusheng
 * @since 2014-2-19
 * @package hm-web.web-api
 */

class PfinalAutoloadLoader{
	
	protected static $reservedPrefix = array('Pfinal');
	
	protected static $nsAutoloader = array();
	
	protected static $loader = null;
	
	/**
	 * (non-PHPdoc)
	 * @see PFinal_Autoload_Interface::autoload()
	 */
	public static function systemAutoloader($className){
		$classNameSegments = explode('_', $className);
		$prefix = $classNameSegments[0];
		$prefix = ucfirst(strtolower($prefix));
		if (in_array($prefix, self::$reservedPrefix)) {
			require_once 'Pfinal/autoload/PfinalAutoloadDefault.class.php';
			self::$loader = new PFinal_Autoload_Default();
			self::$loader->autoload($className);
		}
		else{
			require_once 'Pfinal/autoload/PfinalAutoloadUser.class.php';
			self::$loader = new PFinal_Autoload_User();
			if(self::$loader->autoload($className)!==true){
				return false;
			}
		}
	}
	
	public static function registerAutoloader($ns,Pfinal_Autoload_Interface $autoloader){
		
	}
}

?>