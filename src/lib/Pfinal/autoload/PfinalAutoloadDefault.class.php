<?php
/**
 *
 * @author yusaint
 * @since 2014-2-19
 * @package Pfinal
 */
require_once ('Pfinal/autoload/PfinalAutoloadInterface.class.php');

class Pfinal_Autoload_Default implements Pfinal_Autoload_Interface {
	
	/**
	 * 用来加载系统库函数
	 * @see PFinal_Autoload_Interface::autoload()
	 */
	public function autoload($className){
		$segments = explode('_', $className);
		$fileName = '';
		foreach ($segments as $segment){
			$fileName .= $segment;
		}
		$fileName .='.class.php';
		
		//去掉尾部
		$segments = array_slice($segments, 0,-1);
		$prefix = ucfirst(strtolower(implode(DIRECTORY_SEPARATOR, $segments)));
		$classFile = $prefix.DIRECTORY_SEPARATOR.$fileName;
		$includePath = explode(PATH_SEPARATOR, get_include_path());
		if (!empty($includePath)){
			foreach ($includePath as $path){
				if (is_file($path.DIRECTORY_SEPARATOR.$classFile)){
					require_once $path.DIRECTORY_SEPARATOR.$classFile;
				}
			}
		}
		return false;
	}
}

?>