<?php
/**
 * @author yusaint
 * @since 2014-2-19
 * @package Pfinal
 */
interface Pfinal_Autoload_Interface {
	
	public function autoload($className);
}

?>