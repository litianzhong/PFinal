<?php
/**
 * 
 * @authorhuangyusheng
 * @2014-10-20
 */
require_once 'Pfinal/Pfinal.class.php';

class Pfinal_Remoting_Config{
	
	protected $timeout;
	
	protected $retryTimes;
	
	protected $key;
	
	protected $secret;
	/**
	 * 
	 * @var Pfinal_Remoting_Server
	 */
	protected $servers;
	
	
	/**
	 * 所有参数统一为:
	 * array(
	 * 	'timeout'=>3s,
	 * 	'retryTime'=>3,
	 * 	'key'=>'asdasdaddsdd'
	 * 	'secret'=>'adasddsd34',
	 * 	'server'=>array(
	 * 		array(
	 * 			'host'=>'cart.api.vip.com/getCart/',
	 * 			'port'>80,
	 * 			'key'=>'12313xsdad41asdaasddsd',
	 * 			'secret'=>12313134
	 * 		),
	 * 		.....
	 * 	),
	 * 
	 * )
	 * @param unknown $conf
	 */
	public function __construct($conf){
		$this->timeout = isset($conf['timeout'])?(int)$conf['timeout']:Pfinal::getKernelConfig()->getConstant()->getSocketTimeout();
		$this->retryTimes = isset($conf['retryTime'])?(int)$conf['retryTime']:1;
		$this->key = isset($conf['key'])?$conf['key']:'';
		$this->secret = isset($conf['secret'])?$conf['secret']:'';
		if (!isset($conf['servers'])|| empty($conf['servers'])){
			require_once 'Pfinal/exception/PfinalExceptionArgument.class.php';
			throw new Pfinal_Exception_Argument("configuration is malformed,require servers", -1);
		}
		foreach ($conf['servers'] as $server){
			$host = $server['host'];
			$port = $server['port'];
			if (empty($host)||empty($port)){
				require_once 'Pfinal/exception/PfinalExceptionArgument.class.php';
				throw new Pfinal_Exception_Argument("configuration is malformed,require host or port", -1);
			}
			if (empty($server['key'])){
				$key = empty($this->key)?'':$this->key;
			}
			
			if (empty($server['secret'])){
				$secret = empty($this->secret)?'':$this->secret;
			}
			
			
			$weight = isset($server['weight'])?(int)$server['weight']:1;
			if ($weight===0){
				continue;
			}
			$serverInstance = new Pfinal_Remoting_Server($host, $port, $key, $secret,$weight);
			$this->servers[] = $serverInstance;
		}
		if (empty($this->servers)){
			require_once 'Pfinal/exception/PfinalExceptionArgument.class.php';
			throw new Pfinal_Exception_Argument("configuration is malformed,server list is empty", -1);
		}
		
		
	}
	/**
	 * @return the $timeout
	 */
	public function getTimeout() {
		return $this->timeout;
	}

	/**
	 * @return the $retryTimes
	 */
	public function getRetryTimes() {
		return $this->retryTimes;
	}

	/**
	 * @return the $key
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @return the $secret
	 */
	public function getSecret() {
		return $this->secret;
	}

	/**
	 * @return the $servers
	 */
	public function getServers() {
		uasort($this->servers, array($this,'sortByWeight'));
		return $this->servers;
	}
	
	protected function sortByWeight($arrA,$arrB){
		return $arrA->getWeight()-$arrB->getWeight();
	}

	/**
	 * @param Ambigous <number, the> $timeout
	 */
	public function setTimeout($timeout) {
		$this->timeout = $timeout;
	}

	/**
	 * @param number $retryTimes
	 */
	public function setRetryTimes($retryTimes) {
		$this->retryTimes = $retryTimes;
	}

	/**
	 * @param Ambigous <string, unknown> $key
	 */
	public function setKey($key) {
		$this->key = $key;
	}

	/**
	 * @param Ambigous <string, unknown> $secret
	 */
	public function setSecret($secret) {
		$this->secret = $secret;
	}

	/**
	 * @param Pfinal_Remoting_Server $servers
	 */
	public function setServers($servers) {
		$this->servers = $servers;
	}

	
	
}

/**
 * 
 * @authorhuangyusheng
 * @2014-10-20
 */
class Pfinal_Remoting_Server{
	
	protected $host;
	
	protected $port;
	
	protected $key;
	
	protected $secret;
	
	protected $weight;
	
	public function __construct($host,$port,$key,$secret,$weight = 1){
		$this->host = $host;
		$this->port = $port;
		$this->key = $key;
		$this->secret = $secret;
		$this->weight = $weight;
	}
	/**
	 * @return the $host
	 */
	public function getHost() {
		return $this->host;
	}

	/**
	 * @return the $port
	 */
	public function getPort() {
		return $this->port;
	}

	/**
	 * @return the $key
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @return the $secret
	 */
	public function getSecret() {
		return $this->secret;
	}

	/**
	 * @param field_type $host
	 */
	public function setHost($host) {
		$this->host = $host;
	}

	/**
	 * @param field_type $port
	 */
	public function setPort($port) {
		$this->port = $port;
	}

	/**
	 * @param field_type $key
	 */
	public function setKey($key) {
		$this->key = $key;
	}

	/**
	 * @param field_type $secret
	 */
	public function setSecret($secret) {
		$this->secret = $secret;
	}
	/**
	 * @return the $weight
	 */
	public function getWeight() {
		return $this->weight;
	}

	/**
	 * @param number $weight
	 */
	public function setWeight($weight) {
		$this->weight = $weight;
	}

	
	

	
	
	
	
	
}