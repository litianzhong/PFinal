<?php
abstract class Pfinal_Config_Base {
	
	abstract public function configConstant(Pfinal_Config_Constant $me);
	abstract public function configRoute(Pfinal_Config_Route $me);
	abstract public function configPlugin(Pfinal_Config_Plugin $me);
	abstract public function configExtension(Pfinal_Config_Extension $me);
	abstract public function configInterceptor();
	abstract public function configHandler();
}

?>