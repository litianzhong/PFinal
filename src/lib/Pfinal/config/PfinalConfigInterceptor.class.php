<?php
class Pfinal_Config_Interceptor {
	
	protected $interceptors = array();
	
	public function add(Pfinal_Interceptor_Interface $interceptor){
		array_push($this->interceptors, $interceptor);
	}
	
	public function getInterceptors(){
		return $this->interceptors;
	}
}

?>