<?php
/**
 * 
 * @author yusaint
 * @since 2014-10-24
 */
class Pfinal_Config_Extension {
	// TODO - Insert your code here
	protected $extensionList = array ();
	
	/**
	 * 
	 * @param unknown_type $extDir
	 * @throws Pfinal_Exception_Runtime
	 * @return Pfinal_Extension_Interface
	 */
	public function addExtension($extDir) {
		$lastPos = strrpos ( $extDir, DIRECTORY_SEPARATOR );
		$extensionName = ucfirst ( substr ( $extDir, $lastPos + 1 ) );
		$instanceName = $extensionName . 'Context';
		
		if (! is_file ( $extDir . DIRECTORY_SEPARATOR . $instanceName . '.php' )) {
			throw new Pfinal_Exception_Runtime ( "{$extDir} is not a valid pfinal extension", - 1 );
		}
		
		require_once $extDir . DIRECTORY_SEPARATOR . $instanceName . '.php';
		
		try {
			$reflectionUtil = new ReflectionClass ( $instanceName );
			$instance = $reflectionUtil->newInstance ();
		} catch ( Exception $ex ) {
			throw new Pfinal_Exception_Runtime ( $ex->getMessage (), $ex->getCode (), - 1 );
		}
		array_push ( $this->extensionList, $instance );
		return $instance;
	}
	/**
	 *
	 * @return Pfinal_Extension_Interface
	 */
	public function getExtensionList() {
		return $this->extensionList;
	}
	
	/**
	 *
	 * @param multitype: $extensionList        	
	 */
	public function setExtensionList($extensionList) {
		$this->extensionList = $extensionList;
	}
}

?>