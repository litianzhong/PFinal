<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
class Pfinal_Config_Route{

	protected $uri;
	
	protected $routerStack = array();
	
	protected $routerStackRegex = array();
	
	protected $routeStackMatch = array();
	
	protected $routeStackPrefix = array();
	
	public function addRoute(Pfinal_Route_Interface_Base $router){
		if ($router instanceof Pfinal_Route_Interface_Regex){
			array_push($this->routerStackRegex, $router);
		}
		
		if ($router instanceof Pfinal_Route_Interface_Match){
			array_push($this->routeStackMatch, $router);
		}
		
		if ($router instanceof Pfinal_Route_Interface_Prefix){
			array_push($this->routeStackPrefix,$router);
		}
	}
	
	/**
	 * 添加默认的routerChain
	 * |----------------|
	 * |	regexRouter	|
	 * |----------------|
	 * |    prefixRouter|
	 * |----------------|
	 * |    matchRouter |
	 * |----------------|
	 * |   defaultRouter|
	 * |----------------|
	 */
	public function getRouteStack(){
		foreach ($this->routerStackRegex as $regex){
			array_push($this->routerStack, $regex);
		}
		
		foreach ($this->routeStackPrefix as $prefix){
			array_push($this->routerStack, $prefix);
		}
		
		foreach ($this->routeStackMatch as $match){
			array_push($this->routerStack, $match);
		}
		
		if (empty($this->routerStack)) {
			array_push($this->routerStack,new Pfinal_Route_Default());
		}	
		return $this->routerStack;
	}

	public function setUri($uri){
		$this->uri = $uri;
	}

	public function getUri(){
		return $this->uri;
	}
	
} 
?>