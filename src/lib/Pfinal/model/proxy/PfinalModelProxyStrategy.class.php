<?php
interface Pfinal_Model_Proxy_Strategy{
	
	/**
	 * 
	 * @param Pfinal_Model_Statement $stm
	 * @return Pfinal_Model_Adapter_Interface
	 */
	public function getAdapterProxy(Pfinal_Model_Statement $stm);
}