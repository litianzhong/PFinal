<?php

/** 
 * @author yusaint
 * 
 */
class Pfinal_Model_DDL extends Pfinal_Model_Statement{
	// TODO - Insert your code here
	
	protected $adapter;
	
	protected $opt;
	
	/**
	 * @return the $opt
	 */
	public function getOpt() {
		return $this->opt;
	}

	/**
	 * @param field_type $opt
	 */
	public function setOpt($opt) {
		$this->opt = $opt;
	}
	
	public function desc($tableName){
		$this->opt = 'desc';
		$this->tableName = $tableName;
	}
	
	public function create($sql){
		throw new Pfinal_Exception_Runtime("unimplemented exception!");
	}
	
	public function drop($tableName){
		$this->opt = 'drop';
		$this->tableName = $tableName;
	}
	
	public function truncate($tableName){
		$this->opt = 'truncate';
		$this->tableName = $tableName;
	}
}

?>