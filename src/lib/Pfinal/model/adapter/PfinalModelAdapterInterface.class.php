<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
interface Pfinal_Model_Adapter_Interface {
	
	public function query(Pfinal_Model_Selector $selector);
	
	public function insert(Pfinal_Model_Insert $insertor);
	
	public function execute(Pfinal_Model_Update $updater);
	
	public function call(Pfinal_Model_DDL $ddl);
}

?>