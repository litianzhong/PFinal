<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
interface Pfinal_Model_Optimizer_Interface {
	
	public function optimize(Pfinal_Model_SelectorChain &$chain);
}

?>