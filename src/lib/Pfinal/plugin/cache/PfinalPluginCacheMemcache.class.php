<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-21
 * @project Pfinal
 */
class Pfinal_Plugin_Cache_Memcache implements Pfinal_Plugin_Cache_Interface {
	
	protected $conf;
	
	protected $memcache;
	
	public function __construct($conf){
		$this->conf = $conf;
		$this->memcache = new Memcache();
		foreach ($conf['servers'] as $server){
			$this->memcache->addServer(
					$server['host'],
					$server['port'],
					$server['persistent'],
					$server['weight'],
					$server['timeout']
			);
		}
	}
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Cache_Interface::get()
	 */
	public function get($key, $value = null) {
		// TODO Auto-generated method stub
		return $this->memcache->get($key);
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Cache_Interface::set()
	 */
	public function set($key, $value, $expire = null) {
		// TODO Auto-generated method stub
		return $this->memcache->set($key,$value,MEMCACHE_COMPRESSED,$expire);
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Cache_Interface::delete()
	 */
	public function delete($key) {
		// TODO Auto-generated method stub
		return $this->memcache->delete($key);
	}
	
	// TODO - Insert your code here
}

?>