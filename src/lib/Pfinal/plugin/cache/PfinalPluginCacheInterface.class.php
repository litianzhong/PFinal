<?php
/**
 * 缓存插件的接口
 * @author yusaint
 * @since 2014-3-21
 * @project Pfinal
 */
interface Pfinal_Plugin_Cache_Interface {
	
	public function get($key,$value = null);
	
	public function set($key,$value,$expire = null);
	
	public function delete($key);
}

?>