<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-23
 * @project Pfinal
 */
class Pfinal_Plugin_Orm_Kit {
	private $propertyMapping;
	private $entity;
	public function __construct($entity){
		$this->entity = $entity;
	}	
	
	/**
	 * 
	 * @param unknown_type $row
	 * @return Pfinal_Plugin_Orm_Kit
	 */
	public function build($row){
		if (empty($this->propertyMapping)){
			$class = new ReflectionClass($this->entity);
			$properties = $class->getProperties(ReflectionProperty::IS_PUBLIC);
			if (!empty($properties)){
				foreach ($properties as $property) {
					$this->propertyMapping[$property->getName()] = $property;
				}
			}
		}
		if (!empty($row)){
			$entityObject = new $this->entity;
			$nameMapping  = $entityObject->getNameMapping();
			foreach ($row as $field=>$value){
				$field = isset($nameMapping[$field])?$nameMapping[$field]:$field;
				if (!isset($this->propertyMapping[$field])){
					continue;
				}else{
					$property = $this->propertyMapping[$field];
					$property->setValue($entityObject,$value);
				}
			}
		}	
		return $entityObject;	
	}
}

?>