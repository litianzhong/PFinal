<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-24
 * @project Pfinal
 */
abstract class Pfinal_Plugin_Orm_Entity {
	const STATUS_OK = 0;
	const STATUS_SUSPEND = 1;
	const STATUS_DELETE = 2;
	
	protected $nameMapping = array();
	
	public function getNameMapping(){
		return $this->nameMapping;
	}
}

?>