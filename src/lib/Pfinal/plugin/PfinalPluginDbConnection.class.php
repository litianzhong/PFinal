<?php
/**
 * Pfinal数据库链接默认插件
 * @author huangyusheng
 * @since 2014-3-20
 * @package project_name.package_name
 */
class Pfinal_Plugin_DbConnection implements Pfinal_Plugin_Interface {
	
	protected $strategy;
	
	/**
	 * @return the $adapter
	 */
	public function getAdapter(Pfinal_Model_Statement $stm) {
		return $this->strategy->getAdapterProxy($stm);
	}

	
	/**
	 * 支持主从和分表分库
	 * @param unknown $params
	 */
	public function __construct(Pfinal_Model_Proxy_Strategy $strategy){
		$this->strategy = $strategy;	
	}
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Interface::start()
	 */
	public function start() {
		// TODO Auto-generated method stub
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Plugin_Interface::stop()
	 */
	public function stop() {
		// TODO Auto-generated method stub
	}
	
	// TODO - Insert your code here
}

?>