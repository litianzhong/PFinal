<?php
/**
 * 
 * @author huangyusheng
 * @since 2014-3-5
 * @package project_name.package_name
 */
class PfinalHttpRequest {
	
	protected $request;
	
	protected $get;
	
	protected $post;
	
	protected $server;
	
	protected $params;
	
	/**
	 * 注入框架特定的参数
	 * @param unknown_type $pfinalHttpParams
	 */
	public function __construct(&$pfinalHttpParams){
		$this->params = $pfinalHttpParams;
	}
	/**
	 * 
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function request($key,$value=null){
		if (isset($_REQUEST[$key]))
			return $_REQUEST[$key];
		else
			return $value;
	}
	
	/**
	 * 
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function get($key,$value = null){
		if (isset($_GET[$key]))
			return $_GET[$key];
		else
			return $value;
	}
	
	/**
	 * 
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function post($key,$value = null){
		if (isset($_POST[$key]))
			return $_POST[$key];
		else
			return $value;
	}
	
	/**
	 * 
	 * @param unknown_type $key
	 * @param unknown_type $value
	 * @return unknown
	 */
	public function cookie($key,$value = null){
		if (isset($_COOKIE[$key]))
			return $_COOKIE[$key];
		else
			return $value;
	}
	
	/**
	 * 获取特定格式的参数
	 * www.pfinal.com/a/b/c/p1-p2-p3
	 * @param unknown_type $pos
	 * @return NULL
	 */
	public function getParameter($pos){
		if ($pos>count($this->params))
			return null;
		else{
			
			return $this->params[$pos];
		}
	}
}

?>