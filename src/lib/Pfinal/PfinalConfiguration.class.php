<?php
/**
 * 
 * @author huangyusheng
 * @since 2014-4-15
 * @package project_name.package_name
 */
class PfinalConfiguration {
	// TODO - Insert your code here
	/**
	 *
	 * @var array
	 */
	public static $configDirList = array();
	/**
	 *
	 * @var array
	*/
	public static $configMap;
	
	public static function addConfigDir($configDir){
		self::$configDirList[] = $configDir;
	}
	
	/**
	 * set config item
	 * @param string $key
	 * @param mixed $value
	 */
	public static function write($key, $value){
		self::$configMap[$key] = $value;
	}
	
	/**
	 * get config item
	 * @param string $key
	 */
	public static function read($key){
		if (isset(self::$configMap[$key])){
			return self::$configMap[$key];
		}
		$pos = strpos($key, '.');
		if ($pos !== false){
			$cn = substr($key, 0, $pos);
			foreach (self::$configDirList as $tmpDir){
				
				$cf = $tmpDir.DIRECTORY_SEPARATOR.$cn.'.inc.php';
				
				if (is_file($cf)){
					require_once($cf);
					break;
				}
			}
		}
		if (isset(self::$configMap[$key])){
			return self::$configMap[$key];
		}
		return false;
	}
}

?>