<?php
define('ROOT', dirname(dirname(dirname(__FILE__))));
define('DS', DIRECTORY_SEPARATOR);
define('CONTROLLER', ROOT);
date_default_timezone_set('Asia/Shanghai');

set_include_path(get_include_path().PATH_SEPARATOR.implode(PATH_SEPARATOR,array(
dirname(ROOT)
)));

require_once ROOT.DS.'Pfinal.class.php';


Pfinal::run('test.orm.DBCOnfig');

class DBTest extends PfinalModelAbstract{
	
	public function __construct(){
		//$this->tableName = 'order_electronic_invoice';
		$this->tableName = 'orders';
		$this->databaseName = 'vipshop';
		parent::__construct();
	}
	
	public function testSelect(){
		$dataset = $this->findById(375);
		print_r($dataset);
	}
	
	public function testLimit(){
		$selector = $this->select();
		$selector
			->from('orders')
			->where('money>?', 200)
			->where('id>?', 200)
			->limit(2);
		$dataset = $this->fetchBySelector($selector);
		print_r($dataset);
	}
	
	public function testJoin(){
		$od = $this->select();
		$ei = $this->select();
		
		$od
			->from(array('order_describe','od'),array('buyer'))
			->where('invoice_type=?', 3);
		$ei
			->from(array('order_electronic_invoice','ei'),array('fp_fm'))
			->where('id>=?', 300);
		$query = $this->beginQuery()->start($od)->innerJoin($ei,'od.id=ei.order_id')->limit(2);
		print_r($this->doQuery($query));
	}
}

$test = new DBTest();
$test->testJoin();