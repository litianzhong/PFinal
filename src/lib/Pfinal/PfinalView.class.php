<?php

abstract class PfinalView{

	protected $view;

	protected $httpResponse;


	/**
	 * [__construct description]
	 * @param Pfinal_Render_Abstract $render [description]
	 */
	public function __construct($view,PfinalHttpResponse $httpResponse){
		$this->view = $view;
		$this->httpResponse = $httpResponse;
	}

	public function getView(){
		return $this->view;
	}

	/**
	 * [set description]
	 * @param [type] $k [description]
	 * @param [type] $v [description]
	 */
	public function set($k,$v){
		$this->httpResponse->set($k,$v);
	}



}