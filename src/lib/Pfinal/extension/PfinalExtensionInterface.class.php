<?php
interface Pfinal_Extension_Interface{
	
	public function bootstrap();
	
	public function autoload($classname);
	
}