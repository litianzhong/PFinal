<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
require_once 'Pfinal/PfinalHttpResponse.class.php';
class Pfinal_Render_Error extends Pfinal_Render_Smarty {
	
	protected $code;
	protected $message;
	public function __construct($code,$message){
		parent::__construct();
		$this->code = $code;
		$this->message = $message;
		$this->httpResponse = new PfinalHttpResponse();

	}
	/*
	 * (non-PHPdoc) @see Pfinal_Render_Abstract::render()
	 */
	public function render() {
		// TODO Auto-generated method stub
		$this->httpResponse->set('message',$this->message);
		$this->view=sprintf('%d.html',$this->code);
		parent::render();
	}
	
	// TODO - Insert your code here
}

?>