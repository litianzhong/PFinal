<?php

require_once 'smarty/Smarty.class.php';
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
class Pfinal_Render_Smarty extends Pfinal_Render_Abstract {
	
	protected $smarty = null;
	
	public function __construct(){
		$this->smarty = new Smarty();
		$this->smarty->left_delimiter='{{';
		$this->smarty->right_delimiter='}}';
		$this->smarty->setTemplateDir(SMARTY_TEMPLATE_DIR);
		$this->smarty->debugging = SMARTY_DEBUG_MODE;
	}
	
	/*
	 * (non-PHPdoc) @see Pfinal_Render_Interface::render()
	 */
	public function render() {
		try{
			$this->smarty->assign($this->httpResponse->getPropersMap());
			$this->smarty->display($this->view);
		}catch(Exception $ex){
			print_r($ex);
		}
	}
	
	// TODO - Insert your code here
}

?>