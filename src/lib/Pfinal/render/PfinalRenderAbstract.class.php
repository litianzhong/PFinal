<?php
/**
 * 
 * @author huangyusheng
 * @since 2014-3-6
 * @package project_name.package_name
 */
abstract class Pfinal_Render_Abstract {
	
	protected $view;
	
	protected $httpResponse;
	/**
	 * @return the $httpResponse
	 */
	public function getHttpResponse() {
		return $this->httpResponse;
	}
	
	/**
	 * @param field_type $httpResponse
	 */
	public function setHttpResponse($httpResponse) {
		$this->httpResponse = $httpResponse;
	}
	
	abstract public function render();
	/**
	 * @return the $view
	 */
	public function getView() {
		return $this->view;
	}

	/**
	 * @param field_type $view
	 */
	public function setView($view) {
		$this->view = $view;
	}

}

?>