<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
class PfinalConfig {
	
	protected $constant;
	
	protected $route;
	
	protected $plugin;
	
	protected $extension;
	
	public function __construct(){
		$this->constant = new Pfinal_Config_Constant();
		$this->route = new Pfinal_Config_Route();
		$this->plugin = new Pfinal_Config_Plugin();
		$this->extension = new Pfinal_Config_Extension();
	}
	
	
	public function configSystem(Pfinal_Config_Base $configuration){
		$configuration->configConstant($this->constant);
		$configuration->configRoute($this->route);
		$configuration->configPlugin($this->plugin);
		$configuration->configExtension($this->extension);
		
	}
	/**
	 * @return Pfinal_Config_Constant
	 */
	public function getConstant() {
		return $this->constant;
	}

	/**
	 * @return the $route
	 */
	public function getRoute() {
		return $this->route;
	}
	/**
	 * @return the $plugin
	 */
	public function getPlugin() {
		return $this->plugin;
	}

	/**
	 * @param Pfinal_Config_Plugin $plugin
	 */
	public function setPlugin($plugin) {
		$this->plugin = $plugin;
	}
	/**
	 * @return the Pfinal_Config_Extension
	 */
	public function getExtension() {
		return $this->extension;
	}

	
	
}

?>