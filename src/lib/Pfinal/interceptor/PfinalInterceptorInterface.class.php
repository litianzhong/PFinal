<?php
/**
 * 
 * @author yusaint
 * @since 2014-3-9
 * @project Pfinal
 */
interface Pfinal_Interceptor_Interface {
	
	public function interceptor(Pfinal_Invocation_Handler $handler);
}

?>