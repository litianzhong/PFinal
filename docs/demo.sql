-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.0.51b-community-nt-log - MySQL Community Edition (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2014-04-17 08:46:05
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for wx
CREATE DATABASE IF NOT EXISTS `wx` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wx`;


-- Dumping structure for table wx.wx_account
CREATE TABLE IF NOT EXISTS `wx_account` (
  `id` int(10) NOT NULL auto_increment,
  `userId` int(10) NOT NULL,
  `account` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `status` tinyint(3) NOT NULL,
  `createTime` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `userId` (`userId`,`account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table wx.wx_user
CREATE TABLE IF NOT EXISTS `wx_user` (
  `id` int(10) NOT NULL auto_increment,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(64) NOT NULL,
  `status` tinyint(3) NOT NULL,
  `createTime` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
